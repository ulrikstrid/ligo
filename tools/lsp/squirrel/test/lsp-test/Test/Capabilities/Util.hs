module Test.Capabilities.Util
  ( contractsDir
  ) where

contractsDir :: FilePath
contractsDir = "./test/contracts"
